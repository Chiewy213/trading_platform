﻿using System;

namespace TradingPlatform.Types {
    public struct StockPrice {
        public string Title { get; set; }
        public double Open { get; set; }
        public double Close { get; set; }
        public double Current { get; set; }
    }
}
