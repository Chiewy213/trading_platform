import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout/Layout';
import { Home } from './components/Home/Home';
import { FetchData } from './components/Weather/FetchData';
import { Counter } from './components/Counter/Counter';
import { FetchStockPriceData } from './components/StockPrice/FetchStockPriceData';

import './custom.css'

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route exact path='/' component={Home} />
                <Route path='/counter' component={Counter} />
                <Route path='/weather' component={FetchData} />
                <Route path='/stock-prices' component={FetchStockPriceData} />
            </Layout>
        );
    }
}
