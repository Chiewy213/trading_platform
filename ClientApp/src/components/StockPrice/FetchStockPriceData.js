﻿import React, { Component } from 'react';

export class FetchStockPriceData extends Component {
    constructor(props) {
        super(props);
        this.state = { stocks: [], loading: true };
    }

    componentDidMount() {
        this.populateStockPriceData();
    }

    async populateStockPriceData() {
        const response = await fetch('stockprice');
        const data = await response.json();
        this.setState({ stocks: data, loading: false });
    }

    static renderStockPriceTable(stocks) {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Open (£)</th>
                        <th>Close (£)</th>
                        <th>Current (£)</th>
                    </tr>
                </thead>
                <tbody>
                    {stocks.map(stock =>
                        <tr key={stock.current}>
                            <td>{stock.title}</td>
                            <td>{stock.open + ".00"}</td>
                            <td>{stock.close + ".00"}</td>
                            <td>{stock.current + ".00"}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    render = () => {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : FetchStockPriceData.renderStockPriceTable(this.state.stocks);

        return (
            <div>
                <h1 id="tabelLabel" >Stock prices</h1>
                <p>This component demonstrates fetching data from the server.</p>
                {contents}
            </div>
        );
    }
}