﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradingPlatform.Types;

namespace TradingPlatform.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class StockPriceController : ControllerBase {
        private static readonly string[] Summaries = new[] { "Gold", "Silver", "Oil", "Facebook", "Yahoo", "Microsoft", "Dyson", "Siemans", "Google", "Coal" };
        private readonly ILogger<StockPriceController> _logger;
        private readonly Random random = new();

        public StockPriceController(ILogger<StockPriceController> logger) {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<StockPrice> Get() {
            return Enumerable.Range(1, 5).Select(index => new StockPrice {
                Title = Summaries[this.random.Next(Summaries.Length)],
                Open = this.random.Next(0, 5555),
                Close = this.random.Next(0, 5555),
                Current = this.random.Next(0, 5555)
            })
            .ToArray();
        }
    }
}
